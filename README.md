# README

## Testing endpoints

* GET#documents: Get all documents of api
  > curl GET http://localhost:3000/documents

* POST#documents: Create a new document and extract its tags
  > curl -X POST -d "document[url]=http://globo.com" http://localhost:3000/documents

## Key points
* The Extractor :: Tags should be called as a background job:
  I did not implement a background job because it's a test. If I was implementing this task in production I would do a background job because Extractor :: Tags executes a request to an external service and it can take a long time.
