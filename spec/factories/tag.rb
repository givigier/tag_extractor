FactoryGirl.define do
  factory :tag do
    association :document
    content "<h1>Title</h1>"
  end
end
