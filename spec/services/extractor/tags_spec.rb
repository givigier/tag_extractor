require 'rails_helper'

RSpec.describe Extractor::Tags do
  let(:document){ create(:document, tags: []) }

  before(:each) do
    mock_mechanize(document.url, "
      <html> \\
        <h1>Test1</h1> \\
        <h2>Test2</h2> \\
      </html> \\
    ")
  end

  context "after extracting tags of document" do
    it "should persist tags with document as parent" do
      extractor = Extractor::Tags.new(document)
      extractor.save_tags
      
      expect(document.tags.collect(&:content)).to match_array(["<h1>Test1</h1>", "<h2>Test2</h2>"])
    end
  end
end
