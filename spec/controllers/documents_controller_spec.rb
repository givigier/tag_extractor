require 'rails_helper'

RSpec.describe DocumentsController do
  context "GET#index" do
    let!(:document){ create(:document) }
    let!(:tags){ create_list(:tag, 2, content: "<h2>Test</h2>", document: document) }

    describe "return all documents and tags extracted" do
      before(:each){ get :index }

      it "render documents and tags" do
        expected_response = {
          url: document.url,
          tags: [
            { content: tags[0].content }, { content: tags[1].content }
          ]
        }.with_indifferent_access

        expect(json).to eq [expected_response]
      end

      it "returns ok status" do
        expect(response).to have_http_status(:ok)
      end
    end
  end

  context "POST#create" do
    before(:each) do
      mock_mechanize("http://test.com", "
        <html> \\
          <h1>Test1</h1> \\
          <h2>Test2</h2> \\
        </html> \\
      ")
      post :create, params: { document: document }
    end

    describe "with invalid attributes" do
      let(:document) { attributes_for(:document, url: "") }

      it "render errors of extraction" do
        expected_response = {
          url: ["can't be blank", "is not a valid URL"]
        }.with_indifferent_access

        expect(json).to eq expected_response
      end

      it "returns unprocessable_entity status" do
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end

    describe "with valid attributes" do
      let(:document) { attributes_for(:document, url: "http://test.com") }

      it "render the document created with tags" do
        expected_response = {
          url: "http://test.com",
          tags: [
            { content: "<h1>Test1</h1>" }, { content: "<h2>Test2</h2>" }
          ]
        }.with_indifferent_access

        expect(json).to eq expected_response
      end

      it "returns ok status" do
        expect(response).to have_http_status(:ok)
      end
    end
  end
end
