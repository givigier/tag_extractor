require 'rails_helper'

RSpec.describe Tag do
  context "when validating" do
    let(:tag){ build_stubbed(:tag) }

    it { expect(tag).to validate_presence_of(:document) }
    it { expect(tag).to belong_to(:document) }
  end
end
