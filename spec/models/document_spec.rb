require 'rails_helper'

RSpec.describe Document do
  context "when validating" do
    let(:document){ build_stubbed(:document) }

    it { expect(document).to validate_presence_of(:url) }
    it { expect(document).to have_many(:tags) }

    context "url" do
      it "should be invalid with invalid url" do
        document.url = "test"
        expect(document).to have(1).error_on(:url)
      end

      it "should be invalid with valid local url" do
        document.url = "localhost:3000"
        expect(document).to have(1).error_on(:url)
      end
    end
  end
end
