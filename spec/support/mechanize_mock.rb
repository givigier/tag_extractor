module Mocks
  module MechanizeMock
    def mock_mechanize(url, page_content)
      mocked_page = double("page")
      allow_any_instance_of(Mechanize).to receive(:get).with(url).and_yield(mocked_page)
      allow(mocked_page).to receive(:body){ page_content }
    end
  end
end
