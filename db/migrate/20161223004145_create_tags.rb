class CreateTags < ActiveRecord::Migration[5.0]
  def change
    create_table :tags do |t|
      t.references :document, foreign_key: true, null: false
      t.text :content
    end
  end
end
