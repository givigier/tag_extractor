class Document < ApplicationRecord
  has_many :tags

  validates :url, presence: true, url: { no_local: true}
end
