class Tag < ApplicationRecord
  belongs_to :document

  validates :document, presence: true
end
