class DocumentsController < ApplicationController
  def index
    render json: Document.all.includes(:tags), each_serializer: DocumentSerializer
  end

  def create
    document = Document.new(document_params)
    if document.save
      Extractor::Tags.new(document).save_tags
      render json: document, serializer: DocumentSerializer
    else
      render json: document.errors, status: :unprocessable_entity
    end
  end

  private

  def document_params
    params.require(:document).permit(:url)
  end
end
