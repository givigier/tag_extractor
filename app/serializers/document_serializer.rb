class DocumentSerializer < ActiveModel::Serializer
  attributes :url
  has_many :tags
end
