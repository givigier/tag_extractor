class Extractor::Tags
  def initialize(document)
    @document = document
  end

  def save_tags
    extracted_content = extract_tags "a, h1, h2, h3"
    tags = []
    extracted_content.each{ |c| tags << @document.tags.new(content: c.to_s) }
    Tag.import tags
  end

  private
  def extract_tags(tags_to_be_extracted)
    tags = []
    m = Mechanize.new
    m.get(@document.url) do |page|
      html = Nokogiri::HTML(page.body)
      tags = html.css(tags_to_be_extracted)
    end
    tags
  end
end
